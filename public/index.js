const msgs = [];

const request = async (path) => {
  const res = await fetch(path);
  const msgBox = document.getElementById('messages');
  if (msgs.length >= 15) msgs.shift();
  msgs.push(`- ${(await res.json()).msg}\n`);
  msgBox.innerText = msgs.join('');
};

const deployBtn = document.getElementById('deployBtn');
const runWsButton = document.getElementById('runWsBtn');

deployBtn.onclick = () => {
  request('/deploy');
};

runWsButton.onclick = () => {
  request('/sub');
};
