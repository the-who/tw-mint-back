FROM node:16
WORKDIR /app
# Setup
RUN yarn global add typescript
COPY package.json .
COPY yarn.lock .
# Install dep's
RUN yarn install --frozen-lockfile

ENV PORT 3000

# Build
COPY . .
RUN yarn run build
# test
EXPOSE 3000
CMD [ "yarn", "run", "start:prod" ]
