import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Logger,
  Param,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { FileInterceptor } from '@nestjs/platform-express';
import { AppService } from './app.service';
import { AddAddressSetDto } from './dto/addAddressSet.dto';
import { AddAssetsDto } from './dto/addAssets.dto';
import { AddContractDto } from './dto/addContract.dto';
import { CombineCollectionDto } from './dto/combineCollectionDto';
import { CreateCollectionWithUploadedItemsDto } from './dto/createCollectionWithUploadedItems.dto';
import { IBackCommonConfig } from '../common-back/services/config/interfaces';
import {
  checkAssetsName,
  checkAuthors,
  checkFormats,
  getArchiveName,
} from './utils';
import { NodeEnv } from '../common-back/common/myConfig/interfaces';

@Controller('/api')
export class AppController {
  private readonly logger = new Logger(AppController.name);
  constructor(
    private readonly appService: AppService,
    private readonly config: ConfigService<IBackCommonConfig>,
  ) {}

  @Get('/contracts')
  async getAllContracts() {
    return await this.appService.getAllContracts();
  }
  @Get('/unusedContracts')
  async getUnusedContracts() {
    return await this.appService.getUnusedContracts();
  }
  @Get('/contracts/:id')
  async getContract(@Param() id: number) {
    return await this.appService.getContract(id);
  }
  @Get('/unusedAssets')
  async getAllUnusedAssets() {
    return await this.appService.getAllUnusedAssetsPacks();
  }
  @Get('/allAssets')
  async getAllAssets() {
    return await this.appService.getAllAssetsPacks();
  }
  @Get('/assets/:id')
  async getAssetsPack(@Param() id: number) {
    return await this.appService.getAssetsPack(id);
  }
  @Get('/addresses')
  async getAllAddresses() {
    const result = await this.appService.getAllAddressSets();
    return result;
  }
  @Get('/addresses/:id')
  async getAddressSet(@Param('id') id?: number) {
    return await this.appService.getAddressSet(id);
  }
  @Get('/collections')
  async getAllCollections() {
    return await this.appService.getAllCollections();
  }
  @Get('/collections/:id')
  async getCollection(@Param() id: number) {
    return await this.appService.getCollection(id);
  }

  @Post('/addContract')
  async addContract(@Body() data: AddContractDto) {
    return await this.appService.addContract(data);
  }

  @Post('/deployGCWithUploadedItems')
  async deployGenerativeCollectionContract(
    @Body() data: CreateCollectionWithUploadedItemsDto,
  ) {
    const mintkeyId =
      this.config.get<NodeEnv>('nodeEnv') === 'development' || !data.mintkeyId
        ? 0
        : data.mintkeyId;
    if (!data.storageAddressesId) {
      throw new BadRequestException(`addresses id should be sent`);
    }
    const authors = data.authors.split(',');
    checkAuthors(authors);
    const result = await this.appService.createCollectionWithUploadedItems({
      ...data,
      authors,
      mintkeyId,
    });
    return result;
  }
  @Post('/combineCollection')
  async combineCollection(@Body() data: CombineCollectionDto) {
    const mintkeyId =
      this.config.get<NodeEnv>('nodeEnv') === 'development' || !data.mintkeyId
        ? 0
        : data.mintkeyId;
    const check = (field: string) => {
      if (!data[field]) {
        throw new BadRequestException(`${field} id should be sent`);
      }
    };
    check('contractId');
    check('assetsPackId');
    check('storageAddressesId');
    const result = await this.appService.combineCollection({
      ...data,
      mintkeyId,
    });
    return result;
  }

  @Post('/addAddressSet')
  async addAddressSet(@Body() data: AddAddressSetDto) {
    const { setName, ...other } = data;
    return await this.appService.addAddressSet(setName, other);
  }

  @Post('/addAssets')
  @UseInterceptors(FileInterceptor('file'))
  async addAssets(
    @Body() data: AddAssetsDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    try {
      const origArchiveName = getArchiveName(file.originalname);
      checkAssetsName(origArchiveName);
      checkFormats(data.format);
      const result = await this.appService.addAssetsPack({
        ...data,
        assetsName: origArchiveName,
        file,
      });
      return result;
    } catch (e) {
      this.logger.error(`Err while adding new assets: ${e.message}`);
      throw e;
    }
  }
  // TODO
  // @Post('/createCollectionWithAsstes')
  // @UseInterceptors(FileInterceptor('file'))
  // async createCollectionWithAsstes(
  //   @Body() data: CreateCollectionDto,
  //   @UploadedFile() file: Express.Multer.File,
  // ) {
  //   const mintkeyId =
  //     config.nodeEnv === 'DEV' || !data.mintkeyId ? 0 : data.mintkeyId;
  //   checkName(data.collectionName, 'collection name');
  //   checkFormats(data.format);
  //   const authors = data.authors.split(',');
  //   checkAuthors(authors);
  //   const result = this.appService.createCollection({
  //     ...data,
  //     authors,
  //     file,
  //     mintkeyId,
  //   });
  //   return result;
  // }

  // @Post('/createCollection')
  // async createCollection(@Body() data: CreateCollectionDto) {
  //   if (!data.asstesName) return { message: 'Wrong asset name' };
  //   if (!data.collectionName || data.collectionName.length < 3)
  //     return { message: 'Wrong collection name' };
  //   if (!data.assetGroups) return { message: 'Wrong asset groups' };
  //   // TODO
  // }
}
