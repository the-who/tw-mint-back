import { Format } from '../../common-back/entities/assetspack.entity';

export class CreateCollectionDto {
  format: Format;
  collectionName: string;
  description: string;
  authors: string;
  assetsName: string;
  addressSetId: number;
  mintkeyId?: number;
}
