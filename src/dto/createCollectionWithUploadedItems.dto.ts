export class CreateCollectionWithUploadedItemsDto {
  collectionName: string;
  description: string;
  authors: string;
  storageAddressesId: number;
  assetsPackId: number;
  mintkeyId?: number;
}
