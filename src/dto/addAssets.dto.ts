import { Format } from '../../common-back/entities/assetspack.entity';

export class AddAssetsDto {
  format: Format;
}
