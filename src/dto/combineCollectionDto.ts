export class CombineCollectionDto {
  contractId: number;
  assetsPackId: number;
  storageAddressesId: number;
  mintkeyId?: number;
}
