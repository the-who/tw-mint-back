export class AddAddressSetDto {
  setName: string;
  mintKey: string;
  admin: string;
  minter: string;
  artist: string;
}
