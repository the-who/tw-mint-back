import * as fs from 'fs';
import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { MichelsonMap, TezosToolkit } from '@taquito/taquito';
import { ForgerService } from '../forger/forger.service';
// import { config } from '../app.module';
import { ContractService } from '../../common-back/services/db/contract/contract.service';
import {
  IGCStorage,
  IStorageAddresses,
} from '../../common-back/common/interfaces/common';
import { TezosService } from '../../common-back/services/tezos/tezos.service';
import { IBackCommonConfig } from '../../common-back/services/config/interfaces';
import { TokenStorageService } from '../../src/tokenStorage/tokenStorage.service';
import { MichelsonMapKey } from '@taquito/michelson-encoder';
import { NodeEnv } from '../../common-back/common/myConfig/interfaces';

export interface IDeployGenerativeCollectionParams {
  collectionName: string;
  description: string;
  authors: string[];
  storageAddresses: IStorageAddresses;
  tt: TezosToolkit;
}

export interface IDeployResult {
  address: string;
  code: string;
  id: number;
}
/**
 * TODO:
 * 1. add compilation from different sources
 * 2. add some checks for contracts (tests)
 *
 * Deploy service can deploy new contracts
 ******** ONLY FOR Generative Collections (GC) FOR NOW! *********
 */
@Injectable()
export class DeployService {
  private readonly logger = new Logger(DeployService.name);

  constructor(
    private readonly config: ConfigService<IBackCommonConfig>,
    private readonly tt: TezosService,
    private readonly forger: ForgerService,
    private readonly contract: ContractService,
    private readonly tokenStorage: TokenStorageService,
  ) {}
  getGenerativeNftContract() {
    const contractPath = this.config.get<string>('gCcontractPath');
    const code = fs.readFileSync(contractPath).toString();
    return code;
  }
  /**
   * Deploys contract for generative collection and saves info in DB
   */
  async deployGenerativeCollection(
    params: IDeployGenerativeCollectionParams,
  ): Promise<IDeployResult> {
    const { collectionName, description, authors, storageAddresses, tt } =
      params;
    this.logger.log(`Deploying contract...`);
    const code = this.getGenerativeNftContract();
    const metadataObj = {
      name: collectionName,
      description: description,
      interfaces: ['TZIP-012', 'TZIP-016', 'TZIP-021'],
      authors: authors,
      homepage: 'https://thwh.com',
    };
    const metaPin = await this.tokenStorage.pinJson(metadataObj, {
      name: `${metadataObj.name} project's metadata file`,
    });
    const metadata = await this.forger.forgeCollectionMetadataStoredInIpfs(
      metaPin.IpfsHash,
    );
    const { mintKey, admin, minter, artist } = storageAddresses;
    const storage = this.getInitialStorageValue({
      mint_key: mintKey,
      admin,
      minter,
      artist,
      metadata,
    });
    // const tt = this.tt.getTezosToolkitInstance();
    const address = await this.tt.originateContract({
      originateParams: { code, storage },
      tt,
    });
    const res = await this.contract.addContract({
      address: address,
      code,
    });
    this.logger.log(`Contract ${address} added to DB`);
    return { address, code, id: res.id };
  }
  getInitialStorageValue(params: {
    mint_key: string;
    admin: string;
    minter: string;
    artist: string;
    metadata: MichelsonMap<MichelsonMapKey, any>;
  }): IGCStorage {
    const { mint_key, admin, minter, artist, metadata } = params;
    const env = this.config.get<NodeEnv>('nodeEnv');
    if (env === 'development') {
      return {
        next_id: 1,
        mint_key,
        ledger: MichelsonMap.fromLiteral({}),
        token_metadata: MichelsonMap.fromLiteral({}),
        metadata,
        operators: MichelsonMap.fromLiteral({}),
        paused: false,
        admin,
        minter,
        artist,
        n_sold: 0,
        total: 1000,
      };
    }
    if (env === 'TEST_PROD') {
      return {
        next_id: 1,
        mint_key,
        ledger: MichelsonMap.fromLiteral({}),
        token_metadata: MichelsonMap.fromLiteral({}),
        metadata,
        operators: MichelsonMap.fromLiteral({}),
        paused: false,
        admin,
        minter,
        artist,
        n_sold: 0,
        total: 100,
      };
    }
    throw 'NO PRODUCTION STORAGE VALUE INITIATED!';
  }
}
