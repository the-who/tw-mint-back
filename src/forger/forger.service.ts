import { ConfigService } from '@nestjs/config';
import { Injectable, Logger } from '@nestjs/common';
import {
  BytesLiteral,
  MichelsonMapElt,
  MichelsonMapEltList,
  MichelsonType,
  packDataBytes,
} from '@taquito/michel-codec';
import { MichelsonMap } from '@taquito/michelson-encoder';
import { c2b } from '../../common-back/utils';
import { IBackCommonConfig } from '../../common-back/services/config/interfaces';

/**
 * Service to create different types of
 * michelson structures for taquito
 * @export
 * @class ForgerService
 */
@Injectable()
export class ForgerService {
  private readonly logger = new Logger(ForgerService.name);
  constructor(private readonly config: ConfigService<IBackCommonConfig>) {}

  /**
   * Get token metadata michelson map
   */
  async forgeBytesTokenMetadataStoredInIpfs(
    link: string,
  ): Promise<BytesLiteral> {
    const data: MichelsonMapElt = {
      prim: 'Elt',
      args: [{ string: '' }, c2b(link)],
    };
    const dataObj: MichelsonMapEltList = [data];
    const typeObj: MichelsonType = {
      prim: 'map',
      args: [{ prim: 'string' }, { prim: 'bytes' }],
    };
    const databytes = packDataBytes(dataObj, typeObj);
    return databytes;
  }
  async forgeCollectionMetadataStoredInIpfs(ipfsHash: string) {
    const d = new MichelsonMap({
      prim: 'map',
      args: [{ prim: 'string' }, { prim: 'bytes' }],
    });
    d.set('', c2b(`ipfs://${ipfsHash}`).bytes);
    return d;
  }
}
