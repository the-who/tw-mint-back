import { Test, TestingModule } from '@nestjs/testing';
import { ForgerService } from './forger.service';

describe('ForgerService', () => {
  let service: ForgerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ForgerService],
    }).compile();

    service = module.get<ForgerService>(ForgerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
