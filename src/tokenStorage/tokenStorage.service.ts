import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
// import { NFTStorage, Blob } from 'nft.storage';
// import pinataSDK from '@pinata/sdk';
import { PinataPinResponse, PinataClient, PinataPinOptions } from '@pinata/sdk';
import { IBackCommonConfig } from '../../common-back/services/config/interfaces';
import { Collection } from '../../common-back/entities/collection.entity';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const pinataSDK = require('@pinata/sdk');

@Injectable()
export class TokenStorageService {
  // private readonly nftStorage = new NFTStorage({
  //   token: this.config.get('nftStorageApiKey'),
  // });
  private pinata: PinataClient;
  constructor(private readonly config: ConfigService<IBackCommonConfig>) {
    this.pinata = pinataSDK(
      this.config.get('pinataApiKey'),
      this.config.get('pinataSecret'),
    );
  }
  // async pinFileNftStorage(file: Buffer): Promise<string> {
  //   const blob = new Blob([file]);
  //   const { cid, car } = await NFTStorage.encodeBlob(blob);
  //   await this.nftStorage.storeCar(car, {
  //     onStoredChunk: (size) => console.log(`Stored a chunk of ${size} bytes`),
  //   });
  //   return `ipfs://${cid.toString()}`;
  // }
  async pinFile(file, meta: { name; description }) {
    await this.pinata.testAuthentication();
    const pin = await this.pinata.pinFileToIPFS(file, {
      pinataMetadata: meta,
      pinataOptions: {
        cidVersion: 1,
      },
    });
    return pin;
  }
  async pinFolder(folderPath: string, assetsName: string) {
    const options: PinataPinOptions = {
      pinataMetadata: {
        name: `${assetsName} assets`,
      },
      pinataOptions: {
        cidVersion: 1,
      },
    };
    return await this.pinata.pinFromFS(folderPath, options);
  }
  async pinMetadata(params: {
    collection: Collection;
    metadata: any;
  }): Promise<PinataPinResponse> {
    const { collection, metadata } = params;
    const pin = await this.pinata.pinJSONToIPFS(metadata, {
      pinataMetadata: {
        name: `${collection.name} token metadata file`,
      },
      pinataOptions: {
        cidVersion: 1,
      },
    });
    return pin;
  }
  async pinJson(j, m = {}) {
    return await this.pinata.pinJSONToIPFS(j, {
      pinataMetadata: m,
      pinataOptions: {
        cidVersion: 1,
      },
    });
  }
}
