import { Injectable, Logger } from '@nestjs/common';
import {
  GetObjectCommand,
  GetObjectCommandInput,
  // GetObjectCommandOutput,
  PutObjectCommand,
  PutObjectCommandOutput,
} from '@aws-sdk/client-s3';
// import { Extract } from 'unzipper';
import { TokenStorageService } from '../tokenStorage/tokenStorage.service';
import {
  validateEntries,
  unpack,
  IRandomIds,
  s3client,
  getEntriesFromArchive,
  readableStreamToReadstream,
} from '../utils';
import { Entry } from 'unzipper';
import { AssetsPack } from '../../common-back/entities/assetspack.entity';
import { IPartsObj } from '../graphics/graphics.service';
import * as fs from 'fs/promises';
import { join } from 'path';
import { ConfigService } from '@nestjs/config';
import { IBackCommonConfig } from '../../common-back/services/config/interfaces';
import { IAssetsLocalDirs } from '../../common-back/services/config/config.service';
// import { config } from '../app.module';

export const getSvgResolution = (svgContent: string) => {
  return 'viewBox="0 0 500 2000"'; // TODO
};

const checkIfValidSvg = async (
  e: Entry,
): Promise<{ isValidEntry: boolean; entryId?: string }> => {
  const entryId = e.path.trim();
  let isValidEntry = true;
  const entity = (await e.buffer()).toString().trim();
  const firstSymbols = entity.substring(0, 4);
  isValidEntry = firstSymbols === '<svg';
  if (!isValidEntry) {
    console.log(`Failed entry: ${firstSymbols} | in: ${entryId}`);
  }
  return { isValidEntry, entryId: isValidEntry ? 'none' : entryId };
};

export interface IGetCollectionPartsParams {
  assets: AssetsPack;
  randomIds: IRandomIds;
}

@Injectable()
export class ContentService {
  private readonly logger = new Logger(ContentService.name);
  private readonly bucketName = 'thewhobucket';
  constructor(
    private readonly tokenStorage: TokenStorageService,
    private readonly config: ConfigService<IBackCommonConfig>,
  ) {}
  async unpackAssetsFromZip(params: {
    format: string;
    file: Express.Multer.File;
    tempFolder: string;
  }): Promise<{
    isAllFilesValid: boolean;
    numberOfFiles?: number;
    firstNotValidEntryId?: string | null;
  }> {
    const { format, file, tempFolder } = params;
    // const { originalname } = file;
    // const foldrename = originalname.substr(0, originalname.length - 4);
    try {
      if (format === 'svg') {
        const { isValid, entryId, totalEntries } = await validateEntries(
          file.buffer,
          [checkIfValidSvg],
        );
        await unpack(file.buffer, tempFolder);
        return {
          firstNotValidEntryId: entryId,
          numberOfFiles: totalEntries,
          isAllFilesValid: isValid,
        };
      }
    } catch (e) {
      this.logger.error(e);
      return {
        isAllFilesValid: false,
        firstNotValidEntryId: '',
        numberOfFiles: 0,
      };
    }
  }
  async uploadAssetsArchiveToS3(params: {
    assetsName: string;
    buffer: Buffer;
  }): Promise<PutObjectCommandOutput & { key: string }> {
    // Set the parameters.
    const bucketParams = {
      Bucket: this.bucketName,
      // Specify the name of the new object. For example, 'index.html'.
      // To create a directory for the object, use '/'. For example, 'myApp/package.json'.
      Key: `${params.assetsName}_assets.zip`,
      // Content of the new object.
      Body: params.buffer,
    };
    const data = await s3client.send(new PutObjectCommand(bucketParams));
    if (data.$metadata.httpStatusCode !== 200) {
      this.logger.error(data);
      throw new Error(`Can not upload file to AWS S3`);
    }
    this.logger.log(
      `Successfuly loaded collection assets to Bucket: ${bucketParams.Bucket} | Key: ${bucketParams.Key}`,
    );
    this.logger.log(data);
    return { ...data, key: bucketParams.Key };
  }
  async uploadCollectionAssetsFolderToIpfs(
    folderPath: string,
    assetsName: string,
  ) {
    this.logger.log(`Uploading assets folder to IPFS...`);
    const result = await this.tokenStorage.pinFolder(folderPath, assetsName);
    this.logger.log(`Successfully uploaded assets for ${assetsName}`);
    return result;
  }
  async getCollectionParts(
    params: IGetCollectionPartsParams,
  ): Promise<IPartsObj> {
    try {
      return await this.getCollectionPartsFromFs(params);
    } catch (e) {
      this.logger.error(
        `Can not retrieve parts from FS. Trying to get assets from IPFS...`,
      );
      this.logger.error(e);

      return this.getCollectionPartsFromIpfs(params);
    }
  }
  async getCollectionPartsFromFs(
    params: IGetCollectionPartsParams,
  ): Promise<IPartsObj> {
    const { assets, randomIds } = params;
    const parts: IPartsObj = {};
    const partsPromises = Object.values(randomIds).map(async (filename) => {
      const pathToFile = join(
        this.config.get<IAssetsLocalDirs>('assetsLocalDirs').collectionsAssets,
        assets.name,
        filename,
      );
      const content = await fs.readFile(pathToFile);
      const setId = Object.keys(randomIds).find(
        (k) => randomIds[k] === filename,
      );
      parts[setId] = content;
    });
    await Promise.all(partsPromises);
    return parts;
  }
  async getCollectionPartsFromAws(
    params: IGetCollectionPartsParams,
  ): Promise<IPartsObj> {
    const { assets, randomIds } = params;
    const bucketParams: GetObjectCommandInput = {
      Bucket: this.bucketName,
      Key: assets.bucket,
    };
    const data = await s3client.send(new GetObjectCommand(bucketParams));
    if (data.$metadata.httpStatusCode !== 200) {
      // TODO add handling for this case with retrieval from IPFS
      this.logger.error(data);
      throw new Error(`Can not get file ${assets.bucket} from AWS S3`);
    }
    const body = await readableStreamToReadstream(data.Body);
    const result = await getEntriesFromArchive(body, randomIds);
    this.logger.log(`Got collection parts from AWS`);
    return result;
  }
  async getCollectionPartsFromIpfs(
    params: IGetCollectionPartsParams,
  ): Promise<IPartsObj> {
    throw 'Implement getCollectionFromIpfs !';
  }
}
