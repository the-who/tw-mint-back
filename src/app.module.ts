import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DeployService } from './deploy/deploy.service';
import { GraphicsService } from './graphics/graphics.service';
import { ForgerService } from './forger/forger.service';
import { TokenStorageService } from './tokenStorage/tokenStorage.service';
import { ContentService } from './contentService/content.service';
import { BigmapUpdatesService } from './bigmapUpdates/bigmapUpdates.service';
import { BackCommonConfig } from '../common-back/services/config/config.service';
import { DbModule } from '../common-back/services/db/db.module';
import { TezosService } from '../common-back/services/tezos/tezos.service';
import { configModule } from '../common-back/services/config/config.module';
import { TzktRestService } from '../common-back/services/tzkt/tzktRest.service';
import { TzktWsService } from '../common-back/services/tzkt/tzktWs.service';

// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config();
const config = new BackCommonConfig(process.env);
const dirN = config.testSrv ? '..' : '../..';
@Module({
  imports: [
    TypeOrmModule.forRoot(config.getTypeormConfig()),
    DbModule,
    HttpModule,
    configModule,
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, dirN, 'build'),
    }),
  ],
  controllers: [AppController],
  providers: [
    AppService,
    DeployService,
    // ConfigService,
    GraphicsService,
    TezosService,
    TzktRestService,
    TzktWsService,
    ForgerService,
    TokenStorageService,
    ContentService,
    BigmapUpdatesService,
  ],
})
export class AppModule {}
