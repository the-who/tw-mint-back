import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as sharp from 'sharp';
import { join } from 'path';
import { getSvgResolution } from '../contentService/content.service';
import { Format } from '../../common-back/entities/assetspack.entity';
import { Collection } from '../../common-back/entities/collection.entity';
import { IBackCommonConfig } from '../../common-back/services/config/interfaces';

export interface IPartsObj {
  [setId: string]: Buffer; //sharp.Sharp;
}

@Injectable()
export class GraphicsService {
  private readonly logger = new Logger(GraphicsService.name);
  constructor(private readonly config: ConfigService<IBackCommonConfig>) {}
  // async glueParts(params: {
  //   collectionId: number;
  //   parts: IPartsObj;
  //   format: Format;
  // }): Promise<sharp.Sharp | string> {
  //   try {
  //     const { parts, format, collectionId } = params;
  //     if (format === 'svg') {
  //       return await this.getTokenSvgContent(parts);
  //     } else if (format === 'png') {
  //       throw 'Implement glue method to get PNG token content!';
  //     }
  //   } catch (e) {
  //     this.logger.error(`Error while gluing parts.`);
  //     throw e;
  //   }
  // }
  /**
   * get svg content from given part id's
   * @param parts object with part id's
   * @returns svg content as a string
   */
  async getTokenSvgContent(parts: IPartsObj): Promise<string> {
    let resolutionString = '';
    const res: string = Object.keys(parts)
      .sort()
      .reduce((acc, prev) => {
        const data = parts[prev].toString();
        if (!resolutionString) resolutionString = getSvgResolution(data);
        const tagsArray = data.split('\n');
        const elems = tagsArray.slice(2, -2);
        // return `${prefix}_0`;
        const clearedElem = elems.join('');
        return (acc += clearedElem);
      }, '');
    return `<svg xmlns="http://www.w3.org/2000/svg" ${resolutionString}>${res}</svg>`;
  }
  async getTokenPngContent(params: {
    parts: IPartsObj;
    collectionId: number;
  }): Promise<sharp.Sharp> {
    const { parts, collectionId } = params;
    return await this.compositeSharpImages(parts, collectionId);
  }
  async compositeSharpImages(parts: any, collectionId: number) {
    const ppp = Object.values(parts).map(async (p, i) => {
      // const r = join(
      //   config.assetsLocalDirs.glueTempFolder,
      //   `${collectionId}`,
      //   `${i}`,
      // );
      // const r = await p.toBuffer();
      return {
        input: p, //r,
      };
    });
    // const res = await Promise.all(ppp);
    const img = sharp().resize(500, 2000);
    const ids = Object.keys(parts).sort();
    const sortedPartsP = ids
      .map((id) => parts[id])
      .map((p) => {
        return {
          input: p as Buffer,
          gravity: 'southeast',
          limitInputPixels: false,
        };
      });

    // const sortedParts = await Promise.all(sortedPartsP);
    const cimg = img.composite(sortedPartsP);
    return cimg;
  }
  async createThumbnail(params: {
    tokenPath: string;
    collection: Collection;
    nftId: number;
  }) {
    const { tokenPath, nftId, collection } = params;
    this.logger.log(
      `Creating thumbnail for ${collection.name} collection NFT ${nftId}`,
    );
    // const buffer = Buffer.from(svg);
    const s = sharp(tokenPath);
    // throw 'Implement creation for thumbnail!';
    const png = s.png().resize(87, 350);
    await png.toFile(
      join(
        this.config.get('assetsLocalDirs').collectionTokenThumbnails,
        `${collection.id}`,
        `${nftId}.png`,
      ),
    );
  }
}
