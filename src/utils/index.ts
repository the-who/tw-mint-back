import { Duplex } from 'stream';
import { BadRequestException } from '@nestjs/common';
import { S3Client } from '@aws-sdk/client-s3';
import { Entry, Extract, Parse } from 'unzipper';
import { IAddNewAssetsResult } from '../app.service';
import { IPartsObj } from '../../src/graphics/graphics.service';
import { IAssetsMetadata } from '../../common-back/entities/assetspack.entity';
import { symbols } from '../../common-back/utils';

export const s3client = new S3Client({ region: 'eu-central-1' });

export function bufferToStream(
  buffer: Buffer,
  enc: BufferEncoding = 'utf-8',
): Duplex {
  const stream = new Duplex();
  stream.push(buffer, enc);
  stream.push(null);
  return stream;
}
export const readableStreamToReadstream = (stream): Promise<Duplex> =>
  new Promise((resolve, reject) => {
    const chunks = [];
    stream.on('data', (chunk) => chunks.push(chunk));
    stream.on('error', reject);
    stream.on('end', () => resolve(bufferToStream(Buffer.concat(chunks))));
  });
export const validateEntries = (
  buffer: Buffer,
  validateFnArr: [
    (e: any) => Promise<{ isValidEntry: boolean; entryId?: string }>,
  ],
  params: {
    skipDirs: boolean;
    skipSystemFolders: boolean;
    format: 'svg' | 'png';
  } = { skipDirs: true, skipSystemFolders: true, format: 'svg' },
): Promise<{
  isValid: boolean;
  entryId?: string | null;
  totalEntries: number;
}> => {
  const { skipDirs, skipSystemFolders, format } = params;
  let flag = true;
  return new Promise((resolve, reject) => {
    const stream = bufferToStream(buffer).pipe(Parse());
    let totalEntries = 0;
    let failedEntryId = null;
    stream.on('entry', (entry: Entry) => {
      const p = entry.path.trim();
      validateFnArr.forEach(async (validateFn) => {
        if (skipDirs && entry.type === 'Directory') return;
        if (skipSystemFolders && p.startsWith('__MACOSX')) return;
        if (format === 'svg' && !p.endsWith('.svg')) return;
        const { isValidEntry, entryId } = await validateFn(entry);
        flag = isValidEntry;
        if (!isValidEntry) failedEntryId = entryId;
      });
      // entry.autodrain();
      totalEntries++;
      // if (flag === false) resolve({ isValid: flag, entryId: p });
    });
    stream.on('finish', () =>
      resolve({ isValid: flag, entryId: failedEntryId, totalEntries }),
    );
    stream.on('error', (error) => reject(error));
  });
};
export const unzip = (
  buffer: Buffer,
  foldername: string,
): Promise<Array<any>> => {
  const stream = bufferToStream(buffer).pipe(Parse());
  const entries = [];
  return new Promise((resolve, reject) => {
    stream.on('entry', (entry) => {
      if (entry.type === 'Directory' && entry.path.startsWith(foldername))
        return;
      if (entry.path.startsWith('__MACOSX')) return;
      entries.push(entry);
    });
    stream.on('finish', () => resolve(entries));
    stream.on('error', (error) => reject(error));
  });
};
export const getEntriesFromArchive = async (
  archive: Duplex,
  randomIds: IRandomIds,
): Promise<IPartsObj> => {
  const entryNames: Array<string> = Object.values(randomIds);
  const stream = archive.pipe(Parse());
  const entries: IPartsObj = {};
  return new Promise<IPartsObj>((resolve, reject) => {
    stream.on('entry', async (entry) => {
      if (entry.type === 'Directory') {
        entry.autodrain();
        return;
      }
      const s = entry.path.split('/');
      if (s.lengt > 2) reject(`Wrong path while getting entries from archive!`);
      if (!entryNames.includes(s[1])) {
        entry.autodrain();
        return;
      }
      const setId = Object.keys(randomIds).find(
        (key) => randomIds[key] === s[1],
      );
      const b = await entry.buffer();
      entries[setId] = Buffer.from(b); //sharp(b);
    });
    stream.on('finish', () => resolve(entries));
    stream.on('error', (error) => reject(error));
  });
  // return p;
};
export const unpack = (buffer: Buffer, folderName: string) => {
  const stream = bufferToStream(buffer).pipe(Extract({ path: folderName }));
  return new Promise((resolve, reject) => {
    stream.on('close', () => resolve(true));
    stream.on('error', (error) => reject(error));
  });
};
export const getArchiveName = (nameWithExt: string): string => {
  if (!nameWithExt.endsWith('.zip'))
    throw new BadRequestException(`File should be a ZIP archive`);
  return nameWithExt.substring(0, nameWithExt.length - 4);
};

export const checkFormats = (format: string) => {
  if (!format || format !== 'svg') {
    throw new BadRequestException('Wrong format value');
  }
};
export const checkName = (variable: string, varName: string) => {
  if (!variable || variable.length < 3) {
    throw new BadRequestException(`Wrong ${varName}`);
  }
};
export const checkAssetsName = (variable: string) => {
  const containsWrongSymbols =
    variable.split('').filter((s) => !symbols.includes(s)).length > 0;
  if (!variable || variable.length < 3) {
    throw new BadRequestException(
      `Wrong assets name. Length should be more than 3 symbols.`,
    );
  }
  if (containsWrongSymbols) {
    throw new BadRequestException(
      `Wrong assets name. Assets name should contain only alphanumeric symbols(A-Z,a-z,0-9).`,
    );
  }
};
export const checkAuthors = (authors: any) => {
  if (!Array.isArray(authors)) {
    throw new BadRequestException(`Wrong authors string!`);
  }
  for (const author of authors) {
    if (author.trim().length < 1) {
      throw new BadRequestException(`Author name should be at least 1 symbol`);
    }
  }
};
export const checkAssetsUploadResult = (result: IAddNewAssetsResult) => {
  if (!result || result.firstNotValidEntryId !== null) {
    throw new BadRequestException(
      `Something was wrong with uploaded assets. Failed entry: ${result.firstNotValidEntryId}`,
    );
  }
};
// export const read = (buffer: Buffer) => {
//   const stream = bufferToStream(buffer);

//   return new Promise((resolve, reject) => {
//     let data = '';

//     stream.on('data', (chunk) => (data += chunk));
//     stream.on('end', () => resolve(data));
//     stream.on('error', (error) => reject(error));
//   });
// };
export interface IRandomIds {
  [setId: string]: string;
}

export const getRandomIds = (params: {
  txHash: string;
  nftId: number;
  assetsMetadata: IAssetsMetadata;
}): IRandomIds => {
  const { txHash, nftId, assetsMetadata } = params;

  const { sets } = assetsMetadata;
  const assetsSetsIds = Object.keys(sets)
    .map((i) => parseInt(i))
    .sort();
  const getId = (i: number, total: number, totalParts: number) =>
    (total + (nftId + i)) % totalParts;
  const result = {};
  for (const setId of assetsSetsIds) {
    const TOTAL_PARTS = sets[setId].length;
    let total = 0;
    for (const symbol of txHash) {
      const weight = symbols.indexOf(symbol);
      total += weight ** 2;
    }
    result[setId] =
      assetsMetadata.sets[setId][getId(setId, total, TOTAL_PARTS)];
  }
  return result;
};
