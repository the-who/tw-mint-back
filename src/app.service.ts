import { join } from 'path';
import * as fs from 'fs';
import { ConfigService } from '@nestjs/config';
import {
  BadRequestException,
  Injectable,
  Logger,
  OnApplicationBootstrap,
} from '@nestjs/common';
import { PinataPinResponse } from '@pinata/sdk';
import { TezosToolkit } from '@taquito/taquito';
import { validateAddress, validatePublicKey } from '@taquito/utils';
import { PutObjectCommandOutput } from '@aws-sdk/client-s3';
import { ContentService } from './contentService/content.service';
import {
  DeployService,
  IDeployGenerativeCollectionParams,
  IDeployResult,
} from './deploy/deploy.service';
import { checkAssetsUploadResult } from './utils';
import { CombineCollectionDto } from './dto/combineCollectionDto';
import { acc } from '../common-back/services/config/myTestnetAcc';
import { TzktRestService } from '../common-back/services/tzkt/tzktRest.service';
import { TzktWsService } from '../common-back/services/tzkt/tzktWs.service';
import {
  AssetsPack,
  Format,
  IAssetsMetadata,
} from '../common-back/entities/assetspack.entity';
import { TezosService } from '../common-back/services/tezos/tezos.service';
import { ContractService } from '../common-back/services/db/contract/contract.service';
import { AddressSetService } from '../common-back/services/db/addressSet/addressSet.service';
import { AssetspackService } from '../common-back/services/db/assetspack/assetspack.service';
import { CollectionService } from '../common-back/services/db/collection/collection.service';
import { IBackCommonConfig } from '../common-back/services/config/interfaces';
import { Collection } from '../common-back/entities/collection.entity';
import { Addressset } from '../common-back/entities/addressset.entity';
import { Contract } from '../common-back/entities/contract.entity';
import { BigmapUpdatesService } from './bigmapUpdates/bigmapUpdates.service';
import { IBigMapUpdateData } from '../common-back/common/libs/tzkt/interfaces';
import { IStorageAddresses } from '../common-back/common/interfaces/common';
import { EnvironmentVariables } from './interfaces';

export interface IAddNewAssetsResult {
  isAllFilesValid: boolean;
  numberOfFiles?: number;
  firstNotValidEntryId?: string;
  s3uploadResult: PutObjectCommandOutput & { key: string };
  ipfsUploadResult: PinataPinResponse;
  assetsMetadata: IAssetsMetadata;
}

@Injectable()
export class AppService implements OnApplicationBootstrap {
  private readonly logger = new Logger(AppService.name);
  constructor(
    private readonly config: ConfigService<
      IBackCommonConfig & EnvironmentVariables
    >,
    private readonly deployService: DeployService,
    private readonly bigmapUpdates: BigmapUpdatesService,
    private readonly tzktRest: TzktRestService,
    private readonly tzktWs: TzktWsService,
    private readonly tt: TezosService,
    private readonly content: ContentService,
    private readonly contract: ContractService,
    private readonly addresses: AddressSetService,
    private readonly assets: AssetspackService,
    private readonly collections: CollectionService,
  ) {}
  getHello(): string {
    return 'Hello World!';
  }
  async onApplicationBootstrap() {
    await this.tzktWs.init();
    const collections = await this.collections.getAll();
    this.initLocalFolders(collections);
    for (const collection of collections) {
      const result = await this.initDefaultGCCollection(collection);
      if (!result.success) continue;
      this.logger.log(
        `Collection ${collection.name} initialized.\n================================${collection.name}================================`,
      );
    }
  }
  /**
   * Init local folders for first time
   */
  initLocalFolders(collections: Collection[]) {
    // TODO if collection created, but no local assets - for this moment local assets should be initialized, because ipfsGet/s3get not implemented yet!
    const assetsLocalDirs = this.config.get('assetsLocalDirs');
    for (const folderPathName in assetsLocalDirs) {
      const folderPath = assetsLocalDirs[folderPathName];
      const isFolderCreated = fs.existsSync(folderPath);
      if (!isFolderCreated) {
        fs.mkdirSync(folderPath);
      }
    }
    collections.forEach((collection) =>
      this.initCollectionTokenFolders(collection),
    );
  }
  initCollectionTokenFolders(col: Collection) {
    const assetsLocalDirs = this.config.get('assetsLocalDirs');
    const targetDirs = ['collectionTokenContent', 'collectionTokenThumbnails'];
    for (const dirName of targetDirs) {
      const folderPath = assetsLocalDirs[dirName];
      const p = join(folderPath, `${col.id}`);
      const isColFolderEx = fs.existsSync(p);
      if (!isColFolderEx) fs.mkdirSync(p);
    }
  }
  async initDefaultGCCollection(collection: Collection): Promise<{
    success: boolean;
  }> {
    const params = {};
    if (collection.mintkey) {
      const collectionKey = {}; // get key from DB
      params['importKeyObj'] = collectionKey;
    } else {
      params['account'] = acc;
    }
    const contractAddress = collection.contract.address;
    try {
      const tt = this.tt.getTezosToolkitInstance();
      const handler = this.bigmapUpdates.getDefaultGCCollectionBuyHandler({
        collection,
        tt,
      });
      await this.tt.importKey({ ...params, tt });
      if (collection.assets) {
        await this.tzktWs.subscribeOnNftBuy({
          contractAddress: collection.contract.address,
          handler,
        });
      }
      const skipMissed = this.config.get<boolean>('SKIP_MISSED');
      if (!skipMissed) {
        await this.fillMissedTokens(tt, collection);
      } else {
        this.logger.log(`Skip missed`);
      }
      return { success: true };
    } catch (e) {
      this.logger.error(
        `Error while init collection on contract ${contractAddress}`,
      );
      this.logger.error(e);
      return { success: false };
    }
  }
  async fillMissedTokens(tt: TezosToolkit, collection: Collection) {
    const lastNBuys: IBigMapUpdateData[] = await this.tzktRest.getBigmapUpdates(
      {
        address: collection.contract.address,
        action: 'add_key',
        path: 'ledger',
      },
    );
    const lastNTokenUploads: IBigMapUpdateData[] =
      await this.tzktRest.getBigmapUpdates({
        address: collection.contract.address,
        action: 'update_key',
        path: 'token_metadata',
      });
    const uploadedKeys: number[] = lastNTokenUploads.map((u) =>
      parseInt(u.content.key),
    );
    const missed = lastNBuys.filter((ledgerChange) => {
      const boughtTokenId = parseInt(ledgerChange.content.key);
      return !uploadedKeys.includes(boughtTokenId);
    });
    for (const ledgerChange of missed) {
      await this.bigmapUpdates.handleDefaultGCBuyOperation({
        ledgerChange,
        tt,
        collection,
      });
    }
    this.logger.log(`Missed tokens uploaded.`);
  }
  async getAllContracts() {
    return await this.contract.getAll();
  }
  async getUnusedContracts() {
    return await this.contract.getAllUnused();
  }
  async getContract(id: number) {
    return await this.contract.getById(id);
  }
  async getAllAssetsPacks() {
    return await this.assets.getAll();
  }
  async getAllUnusedAssetsPacks() {
    return await this.assets.getAllUnused();
  }
  async getAssetsPack(id: number) {
    return await this.assets.getById(id);
  }
  async getAllAddressSets() {
    return await this.addresses.getAll();
  }
  async getAddressSet(id: number) {
    return await this.addresses.getById(id);
  }
  async getAllCollections() {
    return await this.collections.getAll();
  }
  async getCollection(id: number) {
    return await this.collections.getById(id);
  }
  async addContract(params: { address: string }) {
    const code = await this.tzktRest.getContractCode(params);
    // const collectionData = await this.tzkt.getCollectionData(params.address);
    const contractData = code ? { ...params, code } : params;
    const result = await this.contract.addContract(contractData);
    return result;
  }
  async combineCollection(params: CombineCollectionDto): Promise<Collection> {
    const { storageAddressesId, assetsPackId, contractId } = params;
    const mintkeyId =
      this.config.get('nodeEnv') === 'development' || !params.mintkeyId
        ? 0
        : params.mintkeyId;
    const tt = await this.getTt(mintkeyId);
    const contract = await this.contract.getById(contractId);
    if (!contract) throw new BadRequestException(`No contract found!`);
    const contractData = await this.tzktRest.getCollectionData({
      address: contract.address,
    });
    const { collectionName, description, authors } = contractData;
    this.logger.log(`Combining collection...`);
    const collection = await this.makeCollection({
      collectionName,
      description,
      authors,
      contractId,
      assetsPackId,
      storageAddressesId,
    });
    const handler = this.bigmapUpdates.getDefaultGCCollectionBuyHandler({
      collection,
      tt,
    });
    await this.tzktWs.subscribeOnNftBuy({
      contractAddress: collection.contract.address,
      handler,
    });
    return collection;
  }
  async makeCollection(params: {
    collectionName: string;
    description: string;
    authors: string[];
    contractId: number;
    storageAddresses?: Addressset;
    storageAddressesId?: number;
    assetsPackId: number;
  }) {
    const {
      collectionName,
      description,
      authors,
      contractId,
      storageAddressesId,
      assetsPackId,
    } = params;
    let storageAddresses = params.storageAddresses;
    if (!storageAddresses) {
      if (!storageAddressesId)
        throw new Error(`No storageAddresses or their ID were provided!`);
      storageAddresses = await this.addresses.getById(storageAddressesId);
    }
    const contract = await this.contract.getById(contractId);
    const assetsPack = await this.assets.getById(assetsPackId);
    const collection = await this.collections.addCollection({
      name: collectionName,
      description,
      authors: authors.join(','),
      contract,
      assets: assetsPack,
      addresses: storageAddresses,
    });
    this.logger.log(`Collection ${collectionName} successfully created!`);
    this.initCollectionTokenFolders(collection);
    return collection;
  }
  async createCollectionWithUploadedItems(
    params: Omit<
      Omit<IDeployGenerativeCollectionParams, 'storageAddresses'>,
      'tt'
    > & {
      storageAddressesId: number;
      assetsPackId?: number;
      mintkeyId: number | 0;
    },
  ): Promise<Collection> {
    const {
      collectionName,
      authors,
      description,
      assetsPackId,
      storageAddressesId,
    } = params;
    this.logger.debug(
      `Creating collection with:\nname: ${collectionName}\ndescription: ${description}\nauthors${authors.join(
        ', ',
      )}\nassetPackId:${assetsPackId}\nstorageAddressesId:${storageAddressesId}`,
    );
    try {
      const storageAddresses = await this.addresses.getById(storageAddressesId);
      const { deployResult, tt } = await this.deployGC({
        ...params,
        storageAddresses,
      });
      const collection = await this.makeCollection({
        collectionName,
        description,
        authors,
        contractId: deployResult.id,
        storageAddresses,
        assetsPackId,
      });
      const handler = this.bigmapUpdates.getDefaultGCCollectionBuyHandler({
        collection,
        tt,
      });
      await this.tzktWs.subscribeOnNftBuy({
        contractAddress: collection.contract.address,
        handler,
      });
      return collection;
    } catch (e) {
      this.logger.error(`Error while deploying contract:`, JSON.stringify(e));
      // this.logger.error(e);
      throw new BadRequestException(
        `Can not deploy contract. ${JSON.stringify(e.message)}`,
      );
    }
  }
  async deployGC(
    params: Omit<IDeployGenerativeCollectionParams, 'tt'> & {
      // storageAddresses?: IStorageAddresses;
      // storageAddressesId: number;
      assetsPackId?: number;
      mintkeyId: number | 0;
    },
  ): Promise<{ deployResult: IDeployResult; tt: TezosToolkit }> {
    const { collectionName, storageAddresses, mintkeyId } = params;
    const paramsObj = {
      ...params,
      storageAddresses,
    };
    this.logger.log(
      `Got assets pack and address set for newly created collection ${collectionName}`,
    );
    const tt = await this.getTt(mintkeyId);
    const deployResult = await this.deployService.deployGenerativeCollection({
      ...paramsObj,
      tt,
    });
    return {
      deployResult,
      tt,
    };
  }
  async getTt(mintkeyId: number) {
    const tt = this.tt.getTezosToolkitInstance();
    if (mintkeyId === 0) {
      const account = acc;
      await this.tt.importKey({
        tt,
        account,
      });
      return tt;
    } else {
      throw new BadRequestException(`Wrong mintkey id`);
    }
  }
  makeAssetPack(params: {
    format: Format;
    assetsName: string;
    result: IAddNewAssetsResult;
  }) {
    const { format, assetsName, result } = params;
    const assetsPack = AssetsPack.fromValue({
      name: assetsName,
      thumbnail: '',
      provenance: result.ipfsUploadResult.IpfsHash,
      bucket: result.s3uploadResult.key,
      format,
      assetsMetadata: result.assetsMetadata,
    });
    return assetsPack;
  }
  async addAssetsPack(params: {
    format: Format;
    assetsName: string;
    file: Express.Multer.File;
  }): Promise<{
    isAllFilesValid: boolean;
    numberOfFiles?: number;
    firstNotValidEntryId?: string;
    s3uploadResult: any;
    ipfsUploadResult: any;
    assetsInfo: AssetsPack;
  }> {
    const { assetsName, format } = params;
    const result = await this.uploadNewAssets(params);
    checkAssetsUploadResult(result);
    const assetsPack = this.makeAssetPack({ assetsName, result, format });
    const addResult = await this.assets.addOne(assetsPack);
    return { ...result, assetsInfo: addResult };
  }
  async addAddressSet(setName: string, params: IStorageAddresses) {
    for (const keyName in params) {
      const value = params[keyName];
      let validateFn = null;
      if (keyName === 'mintKey') {
        validateFn = validatePublicKey;
      } else {
        validateFn = validateAddress;
      }
      const result = validateFn(value);
      if (result !== 3) throw new BadRequestException(`Wrong ${keyName}`);
    }
    const addressSet = Addressset.fromValue(setName, params);
    return await this.addresses.addOne(addressSet);
  }
  async createCollection(params: {
    format: Format;
    collectionName: string;
    description: string;
    authors: string[];
    addressSetId: number;
    assetsName: string;
    mintkeyId: number;
    addressSet?: Addressset;
    file: Express.Multer.File;
  }): Promise<{
    isAllFilesValid: boolean;
    numberOfFiles?: number;
    firstNotValidEntryId?: string;
    s3uploadResult: PutObjectCommandOutput & { key: string };
    ipfsUploadResult: any;
    contractAddress: string;
  }> {
    const {
      collectionName,
      description,
      authors,
      addressSetId,
      addressSet,
      assetsName,
      format,
      mintkeyId,
    } = params;
    if (!addressSetId && !addressSet) {
      this.logger.error(
        `New address set or already created set id must be provided!`,
      );
      return;
    }
    try {
      const result = await this.uploadNewAssets(params);
      checkAssetsUploadResult(result);
      const assetsPack = this.makeAssetPack({
        assetsName: !assetsName ? `${collectionName}_assets` : assetsName,
        result,
        format,
      });
      const addresSet = await this.addresses.getById(addressSetId);
      const tt = await this.getTt(mintkeyId);
      const deployResult = await this.deployService.deployGenerativeCollection({
        collectionName,
        description,
        authors,
        storageAddresses: addresSet,
        tt,
      });
      const contractEntity = Contract.fromValue(deployResult);
      const collectionCreateResult = await this.collections.addCollection({
        name: collectionName,
        description,
        authors: authors.join(','),
        contract: contractEntity,
        assets: assetsPack,
        addresses: addresSet,
      });
      this.logger.log(`Collection ${collectionName} successfully created!`);
      this.logger.verbose(collectionCreateResult);
      return { ...result, contractAddress: deployResult.address };
    } catch (e) {
      this.logger.error(`Error while creating new collection`);
      this.logger.error(JSON.stringify(e));
    }
  }
  async uploadNewAssets(params: {
    format: string;
    // collectionName: string;
    assetsName: string;
    file: Express.Multer.File;
  }): Promise<IAddNewAssetsResult> {
    const assetsName = params.assetsName;
    const assetPack = await this.assets.getByName(assetsName);
    if (!!assetPack) {
      const msg = `Asset pack name already taken!`;
      throw new BadRequestException(msg);
    }
    const collectionsAssetsDir =
      this.config.get('assetsLocalDirs').collectionsAssets;
    const tempFolder = join(collectionsAssetsDir, assetsName);
    const unpackResult = await this.content.unpackAssetsFromZip({
      ...params,
      tempFolder: collectionsAssetsDir,
    });
    // let assetsMetadata: IAssetsMetadata;
    const assetsMetadata = await this.assets.validateAssetsMetadata({
      pathToAssetsFolder: tempFolder,
      archName: assetsName,
    });
    const s3uploadResult = await this.content.uploadAssetsArchiveToS3({
      assetsName,
      buffer: params.file.buffer,
    });
    const ipfsUploadResult: PinataPinResponse =
      await this.content.uploadCollectionAssetsFolderToIpfs(
        tempFolder,
        assetsName,
      );
    // fs.rmdirSync(tempFolder, { recursive: true });
    return {
      ...unpackResult,
      s3uploadResult,
      ipfsUploadResult,
      assetsMetadata,
    };
  }
}
