import { Test, TestingModule } from '@nestjs/testing';
import { BigmapUpdatesService } from './bigmapUpdates.service';

describe('BigmapUpdatesService', () => {
  let service: BigmapUpdatesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BigmapUpdatesService],
    }).compile();

    service = module.get<BigmapUpdatesService>(BigmapUpdatesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
