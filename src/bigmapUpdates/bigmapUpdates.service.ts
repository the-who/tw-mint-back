import { join } from 'path';
import * as fs from 'fs/promises';
import * as fss from 'fs';
import { Injectable, Logger } from '@nestjs/common';
import { PinataPinResponse } from '@pinata/sdk';
import { TezosToolkit } from '@taquito/taquito';
import { ConfigService } from '@nestjs/config';
import { GraphicsService } from '../graphics/graphics.service';
import { getRandomIds } from '../utils';
import { TokenStorageService } from '../tokenStorage/tokenStorage.service';
import { ForgerService } from '../forger/forger.service';
import { Collection } from '../../common-back/entities/collection.entity';
import { ContentService } from '../contentService/content.service';
import { TezosService } from '../../common-back/services/tezos/tezos.service';
import { sign } from '../../common-back/utils';
import { IBackCommonConfig } from '../../common-back/services/config/interfaces';
import { IAssetsLocalDirs } from '../../common-back/services/config/config.service';
import { IBigMapUpdateData } from '../../common-back/common/libs/tzkt/interfaces';

@Injectable()
export class BigmapUpdatesService {
  private readonly logger = new Logger(BigmapUpdatesService.name);
  constructor(
    private readonly config: ConfigService<IBackCommonConfig>,
    private readonly tt: TezosService,
    private readonly graphics: GraphicsService,
    private readonly tokenStorage: TokenStorageService,
    private readonly forger: ForgerService,
    private readonly content: ContentService,
  ) {}
  getDefaultGCCollectionBuyHandler(params: {
    collection: Collection;
    tt: TezosToolkit;
  }) {
    const handler = async (data: IBigMapUpdateData) => {
      await this.handleDefaultGCBuyOperation({ ...params, ledgerChange: data });
    };
    return handler;
  }
  /**
   * ONLY for Generative Collections
   * Actions to do when buy operation was triggered
   * @param ledgerChange object with bigMapUpdate from tzkt
   * @param contractAddress contract address to check update and run actions
   */
  async handleDefaultGCBuyOperation(params: {
    ledgerChange: IBigMapUpdateData;
    collection: Collection;
    tt: TezosToolkit;
  }) {
    const { ledgerChange, collection, tt } = params;
    const contractAddress = collection.contract.address;
    const changeContractAddress = ledgerChange.contract.address;
    // if (contractAddress !== changeContractAddress) return;
    if (!collection.assets) return;
    this.logger.log(
      `Processing BUY operation on contract ${changeContractAddress}`,
    );
    this.logger.verbose(JSON.stringify(ledgerChange));
    const { assets } = collection;
    // get collection assets by contract address (including metadata file with rarity and sections description)
    const { assetsMetadata, format } = assets;
    const rawTokenId = parseInt(ledgerChange.content.key);
    this.logger.log(
      `Raw token id: ${rawTokenId}. Assets AWS key: ${assets.bucket}`,
    );
    try {
      const randomIds = getRandomIds({
        nftId: rawTokenId,
        txHash: ledgerChange.content.hash,
        assetsMetadata: JSON.parse(assetsMetadata),
      });
      this.logger.log(
        `Random ids for token ${rawTokenId} from ${
          collection.name
        } collection: ${JSON.stringify(randomIds)}`,
      );
      // get random parts according to metadata file and hash of TX
      const parts = await this.content.getCollectionParts({
        assets,
        randomIds,
      });

      const tokenContentPath = join(
        this.config.get<IAssetsLocalDirs>('assetsLocalDirs')
          .collectionTokenContent,
        `${collection.id}`,
        `${rawTokenId}.${format}`,
      );
      // combine parts
      if (format === 'png') {
        const tokenContent = await this.graphics.getTokenPngContent({
          parts,
          collectionId: collection.id,
        });
        await tokenContent.toFile(tokenContentPath);
      } else if (format === 'svg') {
        const tokenContent = await this.graphics.getTokenSvgContent(parts);
        await fs.writeFile(tokenContentPath, tokenContent, {
          encoding: 'utf8',
        });
      }
      this.logger.log(`Token content writed to file:\n${tokenContentPath}`);
      // const zippedSvg = this.zipSvg(svgContent);
      await this.graphics.createThumbnail({
        tokenPath: tokenContentPath,
        collection,
        nftId: rawTokenId,
      });
      this.logger.log(
        `Thumbnail created for NFT ${rawTokenId} from collection ${collection.name}`,
      );
      const thumbnailData = await this.getThumbnailIpfsData({
        nftId: rawTokenId,
        collection,
      });
      const thumbnailUri = `ipfs://${thumbnailData.IpfsHash}`;
      this.logger.log(`Thumbnail uploaded: ${thumbnailUri}`);
      const nftData = await this.getNftIpfsData({
        nftId: rawTokenId,
        nftContent: fss.createReadStream(tokenContentPath),
        collection,
      });
      const artifactUri = `ipfs://${nftData.IpfsHash}`;
      this.logger.log(`Artifact's uploaded: ${artifactUri}`);
      const tokenMetadata = await this.getTokenMetadata({
        collection,
        rawTokenId,
        thumbnailUri,
        artifactUri,
      });
      // upload NFT in IPFS
      const tokenMetadataPin = await this.tokenStorage.pinMetadata({
        collection,
        metadata: tokenMetadata,
      });
      const tokenMetaBytes = (
        await this.forger.forgeBytesTokenMetadataStoredInIpfs(
          `ipfs://${tokenMetadataPin.IpfsHash}`,
        )
      ).bytes;
      this.logger.log(`Token metadata pinned: ${tokenMetadataPin.IpfsHash}`);
      const sd = await sign(tt, tokenMetaBytes);
      const sig = sd.prefixSig; // TODO check this
      const bytes = sd.bytes;
      const loadParams = {
        nft_id: rawTokenId,
        nft_data: bytes,
        sig,
        contractAddress,
      };
      this.logger.log(`Data signed. Loading token metadata into blockchain.`);
      await this.tt.load({ ...loadParams, tt });
    } catch (e) {
      this.logger.error(
        `Can not load NFT assets for contract ${ledgerChange.contract.address}`,
      );
      this.logger.error(e);
      // TODO: at least some kind of notification shuold be sent
      // every handler should handle all inner errors, because handlers runned in tzkt lib constructor context
      // throw e;
    }
  }

  async getThumbnailIpfsData(params: {
    nftId;
    collection: Collection;
  }): Promise<PinataPinResponse> {
    const { nftId, collection } = params;
    const meta = {
      name: `${collection.name}`,
      description: `Thumbnail for ${collection.name} #${nftId}`,
    };
    const thumbPath = join(
      this.config.get<IAssetsLocalDirs>('assetsLocalDirs')
        .collectionTokenThumbnails,
      `${collection.id}`,
      `${nftId}.png`,
    );
    // const thumbBuffer = await thumbnail.toFile(thumbPath);
    const thumbStream = await fss.createReadStream(thumbPath);
    const d = await this.tokenStorage.pinFile(thumbStream, meta);
    return d;
  }
  async getNftIpfsData(params: {
    nftId: number;
    nftContent;
    collection: Collection;
  }): Promise<PinataPinResponse> {
    const { nftId, nftContent, collection } = params;
    const meta = {
      name: `${collection.name}`,
      description: `${collection.name} #${nftId}`,
    };
    const d = await this.tokenStorage.pinFile(nftContent, meta);
    return d;
  }
  /**
   * Get token_metadata object value
   */
  async getTokenMetadata(params: {
    rawTokenId: number;
    thumbnailUri: string;
    artifactUri: string;
    collection: Collection;
  }) {
    const { rawTokenId, artifactUri, thumbnailUri, collection } = params;
    const tokenId = this.getTokenId(rawTokenId);
    const meta = {
      name: collection.name,
      description: collection.description,
      symbol: `W${tokenId}`,
      formats: [
        {
          uri: artifactUri,
          mimeType:
            collection.assets.format === 'svg' ? 'image/svg+xml' : 'image/png',
        },
      ],
      creators: collection.authors.split(','),
      artifactUri,
      thumbnailUri,
    };
    return meta;
  }

  getTokenId(raw) {
    const numOfDigits = `${raw}`.length;
    const prefixes = ['00', '0', ''];
    return `${prefixes[numOfDigits - 1]}${raw}`;
  }
}
